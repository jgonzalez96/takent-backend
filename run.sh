export FLASK_APP=app
export FLASK_ENV=development
export FLASK_DEBUG=True
export API_CONFIG_FILE=develop.yaml
export LOG_CONFIG_FILE=logging.yaml
python app.py

#### Importar librería de requests de Takent en Postman

Para importar la librería de requests de Takent en Postman, descargar los siguientes
archivos de este repositorio, ubicados en /docs/postman_library:

- El archivo "Takent.postman_collection.json", es la carpeta con todas las requests
- Los demás archivos que terminan con "postman_environment.json", son los distintos environments,
  que también se deben importar
  
Luego importar los json desde Postman, uno por uno, de la siguiente forma:

![Imagen1](docs/guides/images/postman_import.png)

![Imagen2](docs/guides/images/postman_import2.png)
